# ![Logo](img/youplay_logo.png) Welcome to YouPlay

Do you love music, but hate restrictions?

With **YouPlay** you can search for whatever music in your mind. It will list a number of matching songs from YouTube. You can listen to it and **YouPlay** will download it as an mp3 file.

##### YouPlay in GUI-mode

![YouPlay Screenshot GUI](img/screenshot_gui.png)

##### YouPlay in CLI-mode

![YouPlay Screenshot CLI](img/screenshot_cli.png)

## Installation

I recommend to install **YouPlay** manually as described in the next chapter. A Flatpak package would be nice, if someone could help to build it. The AUR package is outdated; don't install it.

#### Manual Installation

This installation method is still the prefered one, until an installable package is available. Run these commands in a terminal:

```bash
git clone https://codeberg.org/ralfhersel/youplay.git
cd youplay
```

Than install these packages with the package manager of your GNU/Linux distribution:

> `mpv`, `libmpv1`, `ffmpeg`,  `python3-pip`

On Debian-based systems you can do this with one command:

> `sudo apt install mpv libmpv1 ffmpeg python3-pip`

On Arch-based systems: 

> `sudo pacman -S mpv ffmpeg python-pip`

Now install the Python libraries `python-mpv` and `yt-dlp` with:

* `pip3 install --upgrade python-mpv`

* `python3 -m pip install -U yt-dlp`

Last steps:

* make `youplay.py` and `youplay.sh` executable in your file manager
* change the pathes (Exec and Icon) in `youplay.desktop` to your pathes
  (e.g.: /home/yourpath/youplay)
* copy `youplay.desktop` to `/home/user/.local/share/applications`

## Operation Instructions

YouPlay supports two modes: CLI (command line mode) and GUI (graphical mode).

You can start YouPlay in CLI mode like this:

* Navigate in a terminal to your YouPlay directory: `cd /home/blabla/youplay`

* ./youplay.py

* ./youplay.py --help

* ./youplay.py songtitle

All other options will be shown interactively.

You can also start YouPlay in GUI mode:

* Navigate in a terminal to your YouPlay directory: `cd /home/blabla/youplay`
* ./youplay.sh
* or from your application menu or launcher (this requires `youplay.desktop` with adapted pathes, copied to `/home/user/.local/share/applications/`)

When the GUI shows up, you can enter a song title (min 4 characters) that you want to listen to. The App will present a list of 10 matching songs from Youtube. Select one or doubleclick to start downloading and playing it. All songs that you are listened to, are stored as mp3 files in a subfolder named 'youplay' under your standard music folder in home. A song, that was already downloaded, will not be downloaded again, as long as the file exists in the 'youplay' subfolder. You can show and play all downloaded songs with the SONG button. Just enter another search term to return to the search list.

## Problems

Since Youtube changes the API every now and then, it is recommended to update `yt-dlp` regularly:

```bash
yt-dlp -U
```

## Release Notes

###### Version 0.38 - 2022.07.07

- Removed default text in search entry: 'Enter song name'

- Added search icon in search entry

- Added spacing and homogeneous distribution to the buttons

- Chanced window height to show all 10 song lines

- Status bar shows 'song title  -  play time' when playing song

###### Version 0.37 - 2022.05.31

* Force `yt-dlp` to extract best audio in mp3 format. Last version had issues with forcing mp3 format.

###### Version 0.36 - 2022.05.10

* No more webm in between. `yt-dlp` will download mp3 directly.

###### Version 0.35 - 2022.02.14

* Fixed: Special characters (/) in song title caused creation of subdirectories in `/music/youplay/` instead of just saving the song.

###### Version 0.34 - 2022.02.04

* Updating `yt-dlp` will fix the warnings in CLI-mode. You have to run `python3 -m pip install -U yt-dlp` to fight Google's changes in the Youtube API.

* Some warning text changes; not important.

* Removed already out-commented old code; not important.

* Tried GTK4; was not successful. Will try again later.

###### Version 0.33 - 2021.12.08

- Because `youtube-dl` is so much restricted in speed, I switched to `yt-dlp` instead.
- Currently `yt-dlp` throws a warning in CLI-mode. Ignore it, I will fix it later.

###### Version 0.32 - 2021.05.26

- GUI window is now centred on the screen.

###### Version 0.31 - 2021.05.25

* Additional audio controls in GUI-Mode: Pause/Continue, Back, Forward. The Play/Stop and Pause/Continue controls are kind of overlapping in functionality, but who cares.

* Play/Stop will either start a selected song or stop it. If you press Play again, the song will start from the beginning.

* Pause/Continue will either pause the song at the current position, or continue playing at the position where you paused it.

* Back/Forward will step 5 seconds back or forward during playback.

###### Version 0.30 - 2021.05.18

- Until now, all songs were download as webm-files (but named as mp3 files) because youtube-dl ignores the mp3-format parameter. Thats why YouPlay converts all downloaded songs now from webm to mp3.

- I removed the filename sanitizer because I think it is not necessary. We will see if it causes issues with weird song names.

###### Version 0.29 - 2021.03.22

- Improvement of process handling to avoid busy wait on process exit (thank you, Jürgen Hötzel :)

###### Version 0.28 - 2021.01.20

* File 'audio.list' not longer required because of direct search result processing. You can delete the old audio.list (thank you, j.r :)

* Downloaded songs will go to 'youplay' subdirectory under your standard music folder. You can delete or copy the old music subfolder (thank you Joël)

* Search strings must be longer than 3 characters to avoid bullshit searches

* Minor bug fixes

###### Version 0.27 - 2021.01.10

* Duplicated download of existing files fixed again (song.mp3.mp3.mp3)

* Song names are sanitized to avoid invalid search strings in youtube-dl (welle erdball c64)

###### Version 0.26 - 2021.01.08

* Heavy refactoring on the CLI control loop, run ''./youplay.py --help' for more infos

* CLI mode now shows already downloaded songs, run: './'youplay.py --songs'

* Further list alignment for song and search list, both in CLI- and GUI-mode.

* Installation scripts and packages are 'work in progress' and not yet ready.

###### Version 0.25 - 2021.01.04

* Code clean-up

* Mutagen dependency removed (song duration is now fetched with 'ffmpeg')

###### Version 0.24 - 2021.01.03

* GUI song number centered

* Only one button to PLAY or STOP a song

* Doubleclick on a song while another one is playing, will stop the playing song and play the doubleclicked song

* Progress bar works when a second song is searched

* The SONG-button will show and let you play all downloaded songs from the 'music' folder

###### Version 0.23 - 2020.12.23

* GUI mode shows playing time

* Columns and song titles are correctly adjusted to fit into window

###### Version 0.22 - 2020.12.21

* GUI mode will utilize the internal python-mpv player instead of the external mpv player

* PLAY-button will be disabled if you doubleclick song title or click the PLAY-button

###### Version 0.21 - 2020.12.19 (my birthday version)

* ffmpeg dependency removed, because youtube-dl will download mp3 directly

* Downloaded songs will be stored in the 'music' subfolder

* Already downloaded songs will not be downloaded again, but played immediately

* Option to save a song removed from CLI and GUI, because it is obsolete

* PLAY button disabled when song is playing (only when started from PLAY button, not from doubleclick)

###### Version 0.20 - 2020.12.18

* Info about song duration added to CLI and GUI

* Endless streams are omitted (by duration = 0)

* Number of songs reduced to 10 (to avoid lenghty loading time)

###### Version 0.19 - 2020.12.16

* Progress bar while searching, downloading and saving songs
* Progress bar for CLI and GUI mode

###### Version 0.18 - 2020.12.15

* youplay.desktop-file doesn't require bash-script, but call youplay.py directly

* CLI-mode utilises MPV in CLI-mode

* GUI-mode starts MPV in GUI-mode

* Doubleclick in song list, starts download and playback of selected song

###### Earlier versions

* not documented - sorry

## License

Author: Ralf Hersel

License: GPL3

Repository: https://codeberg.org/ralfhersel/youplay.git

## Contact

ralf.hersel@gmx.net
